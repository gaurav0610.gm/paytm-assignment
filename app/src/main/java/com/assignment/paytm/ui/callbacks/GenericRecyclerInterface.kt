package com.assignment.paytm.ui.callbacks

interface GenericRecyclerInterface<LVM, T>{
    fun bindData(binder: LVM, model: T, pos: Int)
}