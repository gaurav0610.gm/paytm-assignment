package com.assignment.paytm.ui.model

data class SubViewData(
    var imageUrl:String?,
    var linkClick:String?,
    var subTitle:String?,
    var title:String?

)