package com.assignment.paytm.ui.model

data class MainViewData(
    var type:String = "",
    var list:ArrayList<SubViewData> = ArrayList()
)