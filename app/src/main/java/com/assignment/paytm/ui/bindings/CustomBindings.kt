package com.assignment.paytm.ui.bindings

import android.graphics.drawable.Drawable
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("newsImage", "newsPlaceholder")
fun newsImageFetcher(view: AppCompatImageView, imageUrl: String?, placeholder: Drawable) {
    Glide.with(view.context)
        .load(imageUrl)
        .placeholder(placeholder)
        .into(view)
}