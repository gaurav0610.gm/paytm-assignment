package com.assignment.paytm.backend.model

data class DataView(
    var title:String,
    var subTitle:String,
    var imageUrl:String,
    var linkClick:String
    )